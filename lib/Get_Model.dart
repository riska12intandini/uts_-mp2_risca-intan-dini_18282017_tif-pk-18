import 'package:http/http.dart' as http;
import 'dart:convert';

class UserGet {
  String id;
  String firstName;
  String lastName;
  String avatar;
  String email;

  UserGet({this.id, this.firstName, this.lastName, this.avatar, this.email});

  factory UserGet.createUserGet(Map<String, dynamic> object) {
    //return object PostResult yang baru
    return UserGet(
      id: object['id'].toString(),
      firstName: object['first_name'],
      lastName: object['last_name'],
      avatar: object['avatar'],
      email: object['email'],
    );
  }

  //metode untuk menghubungkan ke API
  static Future<UserGet> connectToApiUser(String id) async {
    String apiURLPOST = 'https://reqres.in/api/users/' + id;
    //http ini methode async lihat (future)
    // maka menggunakan await
    // calling api
    var apiResult = await http.get(apiURLPOST);

    //untuk mendapat bentuk json
    var jsonObject = json.decode(apiResult.body);

    var userData = (jsonObject as Map<String, dynamic>)['data'];

    //print(userData);

    //kembalikan nilai nya
    return UserGet.createUserGet(userData);
  }
}
