import 'package:mari_beauty/appBar.dart';
import 'package:mari_beauty/colorPick.dart';
import 'package:flutter/material.dart';

import 'models/viewModel.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  List dataUser = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getDataUser();
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        appBar: AllAppBar(),
        body: _viewList(),
        backgroundColor: Colors.black,
      ),
    );
  }

  Widget _viewList() {
    return Container(
        child: dataUser == null
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: dataUser.length,
                itemBuilder: (context, i) {
                  return _Addproduk(dataUser[i]['nama'], dataUser[i]['harga'],
                      dataUser[i]['deskripsi'], dataUser[i]['poto']);
                },
              ));
  }

  Widget _Addproduk(String nama, String harga, String deskripsi, String url) {
    return new Container(
        color: Warna.grey,
        child: Card(
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Image.network(
                  url,
                  // "https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/2020/04/09/663219154.png",
                  fit: BoxFit.cover,
                  width: 400,
                  height: 200,
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                height: 100,
                margin: EdgeInsets.only(left: 8),
                child: Column(
                  children: [
                    Text(
                      nama,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.all_inclusive_sharp,
                          color: Colors.pink[400],
                        ),
                        Text("Kepuasan Pelanggan, Tujuan Kami"),
                        Icon(
                          Icons.all_inclusive_sharp,
                          color: Colors.pink[400],
                        ),
                        SizedBox(
                          width: 20,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                padding: EdgeInsets.only(left: 70, bottom: 15),
                height: 50,
                child: Text(
                  harga,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ));
  }
}
