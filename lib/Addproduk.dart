import 'package:mari_beauty/appBar.dart';
import 'package:mari_beauty/colorPick.dart';
import 'package:mari_beauty/home.dart';
import 'package:mari_beauty/models/contact.dart';
import 'package:mari_beauty/ui/entryform.dart';
import 'package:mari_beauty/ui/viewDaftarProduk.dart';
import 'package:flutter/material.dart';

import 'models/postModel.dart';
import 'models/viewModel.dart';

class AddProduk extends StatefulWidget {
  @override
  _ProdukState createState() => _ProdukState();
}

class _ProdukState extends State<AddProduk> {
  Contact contact;

  TextEditingController _namaProduk = new TextEditingController();
  TextEditingController _hargaProduk = new TextEditingController();
  TextEditingController _deskripsiProduk = new TextEditingController();
  TextEditingController _url_potoProduk = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: _form()),
    );
  }

  Widget _button() {
    return Container(
        child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ViewDaftarProduk()));
            },
            child: Container(
              width: 200,
              padding: EdgeInsets.symmetric(vertical: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.shade200,
                        offset: Offset(2, 4),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Warna.orenMuda, Warna.merahKeterangan])),
              child: Text(
                'Lihat Produk',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          new InkWell(
            onTap: () {
              UserpostModel commRequest = UserpostModel();
              commRequest.namaProduk = _namaProduk.text;
              commRequest.hargaProduk = _hargaProduk.text;
              commRequest.deskripsiProduk = _deskripsiProduk.text;
              commRequest.url_potoProduk = _url_potoProduk.text;

              UserViewModel()
                  .postUser(userpostModelToJson(commRequest))
                  .then((value) => print('success'));
            },
            child: Container(
              width: 200,
              padding: EdgeInsets.symmetric(vertical: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.shade200,
                        offset: Offset(2, 4),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Warna.orenMuda, Warna.merahKeterangan])),
              child: Text(
                'Add Produk',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
          )
        ],
      ),
    ));
  }

  Widget _form() {
    return new Column(
      children: <Widget>[
        new ListTile(
          leading: const Icon(Icons.paste_rounded),
          title: new TextField(
            controller: _namaProduk,
            decoration: new InputDecoration(
              hintText: "Nama Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.money),
          title: new TextField(
            controller: _hargaProduk,
            decoration: new InputDecoration(
              hintText: "Harga Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.money),
          title: new TextField(
            controller: _deskripsiProduk,
            decoration: new InputDecoration(
              hintText: "Deskripsi Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.image),
          title: new TextField(
            controller: _url_potoProduk,
            decoration: new InputDecoration(
              hintText: "Gambar Url produk",
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        new InkWell(
          onTap: () {
            UserpostModel commRequest = UserpostModel();
            commRequest.namaProduk = _namaProduk.text;
            commRequest.hargaProduk = _hargaProduk.text;
            commRequest.deskripsiProduk = _deskripsiProduk.text;
            commRequest.url_potoProduk = _url_potoProduk.text;

            UserViewModel()
                .postUser(userpostModelToJson(commRequest))
                .then((value) => print('success'));

            Navigator.of(context)
                .pushReplacement(new MaterialPageRoute(builder: (_) {
              return new BerandaPage();
            }));
          },
          child: Container(
            width: 200,
            padding: EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Warna.orenMuda, Warna.merahKeterangan])),
            child: Text(
              'Add Produk',
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  Future<Contact> navigateToEntryForm(
      BuildContext context, Contact contact) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return EntryForm(contact);
    }));
    return result;
  }
}
