import 'dart:convert';

List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({
    this.namaProduk,
    this.hargaProduk,
    this.deskripsiProduk,
    this.url_potoProduk,
  });

  String namaProduk;
  String hargaProduk;
  String deskripsiProduk;
  String url_potoProduk;

  Map toJson() => {
        "nama_produk": namaProduk,
        "harga_produk": hargaProduk,
        "deskripsi_produk": deskripsiProduk,
        "url_potoproduk": url_potoProduk,
      };
}
