import 'package:mari_beauty/Data_Diri.dart';
import 'package:mari_beauty/about.dart';
import 'package:mari_beauty/colorPick.dart';
import 'package:mari_beauty/home.dart';
import 'package:mari_beauty/Addproduk.dart';
import 'package:flutter/material.dart';

class BelajarNavBar extends StatefulWidget {
  @override
  _BelajarNavBarState createState() => _BelajarNavBarState();
}

class _BelajarNavBarState extends State<BelajarNavBar> {
  static String tag = 'home-page';
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [
    new BerandaPage(),
    AddProduk(),
    About(),g
    Data_Diri()
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home_work_outlined,
            color: Warna.biruKetuaan,
          ),
          icon: new Icon(
            Icons.home_work_outlined,
            color: Colors.grey,
          ),
          title: new Text(
            'Home',
          ),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.post_add_outlined,
            color: Warna.biruKetuaan,
          ),
          icon: new Icon(
            Icons.post_add_outlined,
            color: Colors.grey,
          ),
          title: new Text('Add Produk'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person_pin_outlined,
            color: Warna.biruKetuaan,
          ),
          icon: new Icon(
            Icons.person_pin_outlined,
            color: Colors.grey,
          ),
          title: new Text('About'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home_work_outlined,
            color: Warna.biruKetuaan,
          ),
          icon: new Icon(
            Icons.account_circle_rounded,
            color: Colors.grey,
          ),
          title: new Text(
            'Data Diri',
          ),
        ),
      ],
    );
  }
}
