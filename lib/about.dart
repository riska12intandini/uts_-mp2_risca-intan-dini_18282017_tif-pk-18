import 'package:mari_beauty/appBar.dart';
import 'package:mari_beauty/colorPick.dart';
import 'package:flutter/material.dart';

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AllAppBar(),
      body: Container(
          padding: EdgeInsets.all(15),
          color: Warna.grey200,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                "@ Risca Intan Dini, 18282017",
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Copyright",
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
              SizedBox(
                height: 80,
              ),
              Center(
                child: Text(
                  "Mari_Beauty",
                  style: TextStyle(color: Colors.blue[600], fontSize: 20),
                ),
              )
            ],
          )),
    ));
  }
}
